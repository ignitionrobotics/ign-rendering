# REPOSITORY MOVED

## This repository has moved to

https://github.com/ignitionrobotics/ign-rendering

## Issues and pull requests are backed up at

https://osrf-migration.github.io/ignition-gh-pages/#!/ignitionrobotics/ign-rendering

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/ign-rendering

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

